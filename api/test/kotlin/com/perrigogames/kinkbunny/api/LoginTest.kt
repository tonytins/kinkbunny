package com.perrigogames.kinkbunny.api

import com.perrigogames.kinkbunny.api.service.LoginResponse
import org.junit.Test
import retrofit2.Response
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class LoginTest: TestScript {

    @Test
    fun loggingInAsGuest() {
        loginGuest { sessionId, login ->
            assertTrue { sessionId.isNotEmpty() }
            testResultBody(login) {
                assertEquals(0, userId)
                assertTrue { ratingsMask.isNotEmpty() }
            }
        }
    }

    @Test
    fun loggingIn_incorrectPassword() {
        login("foo", "bar") { sessionId, login ->
            testResultBody(login) {
                assertTrue { errorMessage!!.isNotEmpty() }
            }
        }
    }

    @Test
    fun loggingIn_correctPassword() {
        login { sessionId, response ->
            assertTrue { sessionId.isNotEmpty() }
            testResultBody(response) {
                assertTrue { ratingsMask.isNotEmpty() }
            }
        }
    }

    private inline fun testResultBody(response: Response<LoginResponse>, block: LoginResponse.() -> Unit) {
        println("${response.message()} (${response.isSuccessful}) -- ${response.body()!!.sessionId}")
        response.body()!!.apply(block)
    }

    private inline fun testResultBody(response: LoginResponse, block: LoginResponse.() -> Unit) {
        println(response.sessionId)
        response.apply(block)
    }
}

