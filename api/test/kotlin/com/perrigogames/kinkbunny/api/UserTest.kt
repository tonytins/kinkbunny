package com.perrigogames.kinkbunny.api

import kotlin.test.assertTrue

class UserTest: TestScript {

    fun getWatchlist_shouldContainUsers() {
        login { sessionId, response ->
            apiAction(Inkbunny.userApi.getUserWatchlist(sessionId)) {
                assertTrue { it.resultsCount > 0 }
                assertTrue { it.watches.isNotEmpty() }
            }
        }
    }

//    fun changeAllowedRatings_
}