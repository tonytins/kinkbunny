package com.perrigogames.kinkbunny.api

import com.perrigogames.kinkbunny.api.service.LoginResponse
import kotlinx.coroutines.experimental.Deferred
import kotlinx.coroutines.experimental.runBlocking

interface TestScript {

    fun loginGuest(callback: (String, LoginResponse) -> Unit) =
            apiAction(Inkbunny.loginApi.loginAsGuest()) { callback(it.sessionId, it) }

    fun login(username: String = USERNAME,
              password: String = PASSWORD,
              callback: (String, LoginResponse) -> Unit) =
            apiAction(Inkbunny.loginApi.login(username, password)) {
                callback(it.sessionId, it)
                logout(it.sessionId)
            }

    private fun logout(sessionId: String) = apiAction(Inkbunny.loginApi.logout(sessionId))

    fun <T>apiAction(call: Deferred<T>, callback: ((T) -> Unit)? = null) = runBlocking { callback?.invoke(call.await()) }

    companion object {
        const val USERNAME = "username"
        const val PASSWORD = "password"
    }
}