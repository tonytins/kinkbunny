package com.perrigogames.kinkbunny.api

import org.junit.Test
import kotlin.test.assertTrue

class SuggestionTest: TestScript {

    @Test
    fun testKeywordSugg_hasResults() {
        apiAction(Inkbunny.suggestionApi.getKeywordSuggestions("fem")) {
            assertTrue { it.filter { it.singleWord.equals("female", true) }.size > 2 }
        }
    }

    @Test
    fun testUsernameSugg_hasResults() {
        apiAction(Inkbunny.suggestionApi.getUsernameSuggestions("aog")) {
            assertTrue { it.filter { it.singleWord.equals("aogami", true) }.size == 1 }
        }
    }

    @Test
    fun tetKeywordSugg_gibberish() {
        apiAction(Inkbunny.suggestionApi.getKeywordSuggestions("jgndddlg")) {
            assertTrue { it.isEmpty() }
        }
    }

    @Test
    fun testUsernameSugg_gibberish() {
        apiAction(Inkbunny.suggestionApi.getUsernameSuggestions("jgndddlg")) {
            assertTrue { it.isEmpty() }
        }
    }
}