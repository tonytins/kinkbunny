package com.perrigogames.kinkbunny.api

import com.google.gson.annotations.SerializedName

/**
 * A base request class containing everything that is required for most
 * requests.
 */
open class BaseRequest(
        @SerializedName("sid") val sessionId: String)