package com.perrigogames.kinkbunny.api

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.experimental.CoroutineCallAdapterFactory
import com.perrigogames.kinkbunny.api.service.*
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object Inkbunny {

    private val retrofit = Retrofit.Builder()
            .baseUrl("https://inkbunny.net/")
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .build()

    val loginApi = retrofit.create(LoginService::class.java)
    val submissionApi = retrofit.create(SubmisionService::class.java)
    val suggestionApi = retrofit.create(SuggestionService::class.java)
    val userApi = retrofit.create(UserService::class.java)
}
