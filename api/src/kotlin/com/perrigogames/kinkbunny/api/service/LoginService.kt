package com.perrigogames.kinkbunny.api.service

import com.google.gson.annotations.SerializedName
import com.perrigogames.kinkbunny.api.BaseResponse
import kotlinx.coroutines.experimental.Deferred
import retrofit2.http.POST
import retrofit2.http.Query

interface LoginService {

    @POST("api_login.php?username=guest")
    fun loginAsGuest(): Deferred<LoginResponse>

    @POST("api_login.php")
    fun login(
            @Query("username") username: String,
            @Query("password") password: String): Deferred<LoginResponse>

    @POST("api_logout.php")
    fun logout(
            @Query("sid") sid: String): Deferred<LogoutResponse>
}

class LoginResponse(
        @SerializedName("user_id") val userId: Int,
        @SerializedName("ratingsmask") val ratingsMask: String) : BaseResponse()

class LogoutResponse(
        @SerializedName("logout") val logout: String) : BaseResponse()