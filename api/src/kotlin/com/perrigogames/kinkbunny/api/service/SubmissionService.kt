package com.perrigogames.kinkbunny.api.service

import com.google.gson.annotations.SerializedName
import com.perrigogames.kinkbunny.api.BaseResponse
import com.perrigogames.kinkbunny.api.enums.SubmissionType
import kotlinx.coroutines.experimental.Deferred
import retrofit2.http.POST
import retrofit2.http.Query

interface SubmisionService {

    @POST("api_search.php")
    fun performSearch(
            @Query("sid") sessionId: String,
            @Query("submission_ids_only") idsOnly: Boolean? = null,
            @Query("submissions_per_page") submissionsPerPage: Int? = null,
            @Query("page") page: Int? = null,
            @Query("keywords_list") keywordsList: Boolean? = null,
            @Query("no_submissions") noSubmissions: Boolean? = null,
            @Query("get_rid") getResultId: Boolean? = null,
            @Query("field_join_type") fieldJoinType: String? = null,
            @Query("text") text: String? = null,
            @Query("string_join_type") stringJoinType: String? = null,
            @Query("keywords") keywords: Boolean? = null,
            @Query("title") title: Boolean? = null,
            @Query("description") description: Boolean? = null,
            @Query("md5") md5: Boolean? = null,
            @Query("keyword_id") keywordId: String? = null,
            @Query("username") username: String? = null,
            @Query("user_id") userId: String? = null,
            @Query("favs_user_id") favesUserId: String? = null,
            @Query("unread_submissions") unreadSubmissions: Boolean? = null,
            @Query("type") type: SubmissionType? = null,
            @Query("sales") sales: String? = null,
            @Query("pool_id") poolId: String? = null,
            @Query("orderby") orderBy: Boolean? = null,
            @Query("dayslimit") daysLimit: Int? = null,
            @Query("random") random: Boolean? = null,
            @Query("scraps") scraps: String? = null,
            @Query("count_limit") countLimit: Int? = null): Deferred<BaseResponse>

    @POST("api_search.php")
    fun resumeSearch(
            @Query("sid") sessionId: String,
            @Query("rid") resultId: String? = null,
            @Query("submission_ids_only") idsOnly: Boolean? = null,
            @Query("submissions_per_page") submissionsPerPage: Int? = null,
            @Query("page") page: Int? = null,
            @Query("keywords_list") keywords: Boolean? = null,
            @Query("no_submissions") noSubmissions: Boolean? = null,
            @Query("get_rid") getResultId: Boolean? = null): Deferred<BaseResponse>

    @POST("api_submissions.php")
    fun getSubmissionDetails(): Deferred<BaseResponse>

    @POST("api_submissionfavingusers.php")
    fun getSubmissionFaves(
            @Query("sid") sessionId: String,
            @Query("submission_id") submissionId: Int): Deferred<SubmissionFavesResponse>

    @POST("api_upload.php")
    fun uploadSubmission(
            @Query("sid") sessionId: String,
            @Query("submission_id") submissionId: String? = null,
            @Query("progress_key") progressKey: String? = null,
            @Query("notify") notify: Boolean? = null,
            @Query("replace") replace: String? = null,
            @Query("uploadedfile") uploadedFile: Any? = null,
            @Query("uploadedthumbnail") uploadedThumbnail: Any? = null,
            @Query("zipfile") zipfile: Any? = null): Deferred<UploadSubmissionResponse>

    @POST("api_progress.php")
    fun getUploadProgress(
            @Query("progress_key") progressKey: String,
            @Query("cancel") cancel: Boolean): Deferred<UploadProgressResponse>

    @POST("api_delsubmission.php")
    fun deleteSubmission(
            @Query("sid") sid: String,
            @Query("submission_id") submissionId: String): Deferred<DeleteSubmissionResponse>

    @POST("api_delfile.php")
    fun deleteFile(
            @Query("sid") sid: String,
            @Query("file_id") fileId: String): Deferred<DeleteFileResponse>

    @POST("api_reorderfile.php")
    fun reorderFiles(
            @Query("sid") sid: String,
            @Query("file_id") fileId: String,
            @Query("newpos") newPosition: Int): Deferred<ReorderFileResponse>

    @POST("api_editsubmission.php")
    fun editSubmission(
            @Query("sid") sid: String,
            @Query("submission_id") submissionId: String,
            @Query("title") title: String? = null,
            @Query("desc") description: String? = null,
            @Query("story") story: String? = null,
            @Query("convert_html_entities") convertHtmlEntities: Boolean? = null,
            @Query("type") type: SubmissionType? = null,
            @Query("scraps") scraps: Boolean? = null,
            @Query("use_twitter") useTwitter: Boolean? = null,
            @Query("twitter_image_pref") twitterImagePref: String? = null, //TODO
            @Query("visibility") visibility: String? = null, //TODO
            @Query("keywords") sid: String? = null,
            @Query("tag") tag: Array<Boolean>? = null,
            @Query("guest_block") blockGuests: Boolean? = null,
            @Query("friends_only") friendsOnly: Boolean? = null): Deferred<EditSubmissionResponse>
}

data class FavingUser(
        @SerializedName("user_id") val userId: String,
        @SerializedName("username") val username: String)

data class SubmissionFavesResponse(
        @SerializedName("favingusers") val users: List<FavingUser>) : BaseResponse()

data class UploadSubmissionResponse(
        @SerializedName("submission_id") val submissionId: String): BaseResponse()

data class UploadProgressResponse(
        @SerializedName("status") val status: Any,
        @SerializedName("filescount") val filesCount: Int,
        @SerializedName("filescompletecount") val filesCompleteCount: Int,
        @SerializedName("curfilename") val currFileName: String,
        @SerializedName("lastuserresponse_epoch_secs") val lastUserResponseEpochSecs: Long,
        @SerializedName("usercancelled") val userCancelled: Boolean): BaseResponse()

data class DeleteSubmissionResponse(
        @SerializedName("submission_id") val submissionId: String): BaseResponse()

data class DeleteFileResponse(
        @SerializedName("submission_id") val submissionId: String,
        @SerializedName("file_id") val fileId: String): BaseResponse()

data class ReorderFileResponse(
        @SerializedName("submission_id") val submissionId: String,
        @SerializedName("file_id") val fileId: String,
        @SerializedName("newpos") val newPosition: Int): BaseResponse()

data class EditSubmissionResponse(
        @SerializedName("submission_id") val submissionId: String,
        @SerializedName("twitter_authentication_success") val twitterAuthSuccess: Boolean): BaseResponse()