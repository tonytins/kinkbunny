package com.perrigogames.kinkbunny.api.service

import com.google.gson.annotations.SerializedName
import kotlinx.coroutines.experimental.Deferred
import retrofit2.http.POST
import retrofit2.http.Query

interface SuggestionService {

    @POST("api_search_autosuggest.php")
    fun getKeywordSuggestions(
            @Query("keyword") keyword: String,
            @Query("ratingsmask") ratingsMask: String = "1",
            @Query("underscorespaces") underscoreSpaces : Boolean?) : Deferred<List<Suggestion>>

    @POST("api_username_autosuggest.php")
    fun getUsernameSuggestions(
            @Query("username") keyword: String,
            @Query("searchtype") searchType: UsernameSearchType = UsernameSearchType.ANY) : Deferred<List<Suggestion>>
}

enum class UsernameSearchType {
    START, ANY;

    override fun toString() = super.toString().toLowerCase()
}

data class Suggestion(
        @SerializedName("id") val id: String,
        @SerializedName("value") val value: String,
        @SerializedName("icon") val icon: String,
        @SerializedName("info") val info: String,
        @SerializedName("singleword") val singleWord: String,
        @SerializedName("searchterm") val searchTerm: String,
        @SerializedName("submissions_count") val submissionsCount: String)