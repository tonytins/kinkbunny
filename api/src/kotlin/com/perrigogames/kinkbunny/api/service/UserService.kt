package com.perrigogames.kinkbunny.api.service

import com.google.gson.annotations.SerializedName
import com.perrigogames.kinkbunny.api.BaseResponse
import kotlinx.coroutines.experimental.Deferred
import retrofit2.http.POST
import retrofit2.http.Query

interface UserService {

    @POST("api_watchlist.php")
    fun getUserWatchlist(
            @Query("sid") sessionId: String,
            @Query("orderby") orderBy: UserSort? = null,
            @Query("limit") limit: Int? = null) : Deferred<WatchlistResponse>

    @POST("api_userrating.php")
    fun setAllowedRatings(
            @Query("sid") sessionId: String,
            @Query("tag[2]") nudity: Boolean? = null,
            @Query("tag[3]") violence: Boolean? = null,
            @Query("tag[4]") sexualContent: Boolean? = null,
            @Query("tag[5]") strongViolence: Boolean? = null): Deferred<BaseResponse>
}

enum class UserSort(val apiKey: String) {
    Alphabetical("alphabetical"),
    CreateDateTime("create_datetime");

    override fun toString() = apiKey
}

class Watch(
        @SerializedName("user_id") val userId: String,
        @SerializedName("username") val username: String)

class WatchlistResponse(
        @SerializedName("results_count") val resultsCount: Int,
        @SerializedName("watches") val watches: List<Watch>) : BaseResponse()