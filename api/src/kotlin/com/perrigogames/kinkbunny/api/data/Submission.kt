package com.perrigogames.kinkbunny.api.data

import com.google.gson.annotations.SerializedName
import com.perrigogames.kinkbunny.api.enums.RatingId
import com.perrigogames.kinkbunny.api.enums.SubmissionType
import java.util.*

data class Submission(
        @SerializedName("submission_id") val id: String,
        @SerializedName("keywords") val keywords: List<Keyword>,
        @SerializedName("hidden") val hidden: Boolean,
        @SerializedName("scraps") val scraps: Boolean,
        @SerializedName("favorite") val favorite: Boolean,
        @SerializedName("favorites_count") val favoritesCount: Int,
        @SerializedName("create_datetime") val createDate: Date,
        @SerializedName("create_datetime_usertime") val userCreateDate: Date,
        @SerializedName("last_file_update_datetime") val lastUpdateDate: Date,
        @SerializedName("last_file_update_datetime_usertime") val userLastUpdateDate: Date,
        @SerializedName("username") val username: String,
        @SerializedName("user_id") val userId: String,
        @SerializedName("user_icon_file_name") val userIconName: String,
        @SerializedName("user_icon_url_(SIZE)") val userIcon: String, //TODO: (SIZE)
        @SerializedName("file_name") val filename: String,
        @SerializedName("latest_file_name") val latestFilename: String,
        @SerializedName("thumbnail_url_(SIZE)") val thumbnailUrl: String, //TODO: (SIZE)
        @SerializedName("thumbnail_url_(SIZE)_noncustom") val noncustomThumbnailUrl: String, //TODO: (SIZE)
        @SerializedName("file_url_(SIZE)") val fileUrl: String, //TODO: (SIZE)
        @SerializedName("latest_thumbnail_url_(SIZE)") val latestThumbnailUrl: String, //TODO: (SIZE)
        @SerializedName("latest_thumbnail_url_(SIZE)_noncustom") val latestNoncustomThumbnailUrl: String, //TODO: (SIZE)
        @SerializedName("latest_file_url_(SIZE)") val latestFileUrl: String, //TODO: (SIZE)
        @SerializedName("files") val files: List<File>,
        @SerializedName("pools") val pools: List<Pool>,
        @SerializedName("description") val description: String,
        @SerializedName("description_bbcode_parsed") val parsedDescription: String,
        @SerializedName("writing") val writing: String,
        @SerializedName("writing_bbcode_parsed") val parsedWriting: String,
        @SerializedName("pools_count") val poolsCount: Int,
        @SerializedName("title") val title: String,
        @SerializedName("deleted") val deleted: Boolean,
        @SerializedName("public") val public: Boolean,
        @SerializedName("mimetype") val mimetype: String,
        @SerializedName("pagecount") val pageCount: Int,
        @SerializedName("latest_mimetype") val latestMimetype: String,
        @SerializedName("rating_id") val ratingId: RatingId,
        @SerializedName("rating_name") val ratingName: String,
        @SerializedName("ratings") val ratings: List<Rating>,
        @SerializedName("thumb_(SIZE)_x") val thumbWidth: Int, //TODO: (SIZE)
        @SerializedName("thumb_(SIZE)_y") val thumbHeight: Int, //TODO: (SIZE)
        @SerializedName("thumb_(SIZE)_noncustom_x") val thumbNoncustomWidth: Int, //TODO: (SIZE)
        @SerializedName("thumb_(SIZE)_noncustom_y") val thumbNoncustomHeight: Int, //TODO: (SIZE)
        @SerializedName("latest_thumb_(SIZE)_x") val latestThumbWidth: Int, //TODO: (SIZE)
        @SerializedName("latest_thumb_(SIZE)_y") val latestThumbHeight: Int, //TODO: (SIZE)
        @SerializedName("latest_thumb_(SIZE)_noncustom_x") val latestThumbNoncustomWidth: Int, //TODO: (SIZE)
        @SerializedName("latest_thumb_(SIZE)_noncustom_y") val latestThumbNoncustomHeight: Int, //TODO: (SIZE)
        @SerializedName("submission_type_id") val submissionType: SubmissionType,
        @SerializedName("type_name") val submissionTypeName: String,
        @SerializedName("guest_block") val blockGuests: Boolean,
        @SerializedName("friends_only") val friendsOnly: Boolean,
        @SerializedName("comments_count") val commentCount: Int,
        @SerializedName("views") val views: Int,
        @SerializedName("sales_description") val salesDescription: String,
        @SerializedName("forsale") val forSale: Boolean,
        @SerializedName("digitalsales") val digitalForSale: Boolean,
        @SerializedName("printsales") val printForSale: Boolean,
        @SerializedName("digital_price") val digitalPrice: String,
        @SerializedName("prints") val prints: List<Print>)

data class Keyword(
        @SerializedName("keyword_id") val id: String,
        @SerializedName("keyword_name") val name: String,
        @SerializedName("contributed") val contributed: String,
        @SerializedName("submissions_count") val submissionCount: Int)

 class File() //TODO

 class Pool() //TODO

 class Rating() //TODO

data class Print(
        @SerializedName("print_size_id") val sizeId: Int,
        @SerializedName("name") val name: String,
        @SerializedName("price") val price: String,
        @SerializedName("price_owner_discount") val ownerPrice: String)