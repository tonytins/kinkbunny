package com.perrigogames.kinkbunny.api

import com.google.gson.annotations.SerializedName

/**
 * A base response class containing everything that the API could return
 * in any response.
 */
open class BaseResponse(
        @SerializedName("sid") val sessionId: String = "",
        @SerializedName("error_code") val errorCode: String? = null,
        @SerializedName("error_message") val errorMessage: String? = null) {

    val isError get() = errorCode != null
}