package com.perrigogames.kinkbunny.api.enums

enum class PrintSize {
    SZ5_5_8_5, // 0
    SZ8_5_11,
    SZ18x24,
    SZ24x36 // 3
}