package com.perrigogames.kinkbunny.api.enums

enum class RatingId {
    GENERAL, MATURE, ADULT
}