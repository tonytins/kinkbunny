@file:Suppress("EXPERIMENTAL_FEATURE_WARNING")

package com.perrigogames.kinkbunny.android

import android.app.Activity
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.widget.Toast
import com.perrigogames.kinkbunny.android.databinding.ActivityLoginBinding
import com.perrigogames.kinkbunny.api.Inkbunny
import com.perrigogames.kinkbunny.api.service.LoginResponse
import com.perrigogames.kinkbunny.api.service.WatchlistResponse
import kotlinx.coroutines.experimental.Deferred
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch

class LoginActivity : Activity() {

    private lateinit var binding: ActivityLoginBinding
    private var viewModel: LoginViewModel = LoginViewModel()

    private val username get() = binding.fieldUsername.text.toString()
    private val password get() = binding.fieldPassword.text.toString()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        binding.activity = this
        binding.viewModel = viewModel
    }

    fun loginClicked() {
        if (username.isNotEmpty() && password.isNotEmpty()) {
            launch(UI) {
                val loginResponse = doLogin(Inkbunny.loginApi.login(username, password), username)
                if (!loginResponse.isError) {
                    checkError("Logged in as $username")
                    val watchResponse = doWatchlistCheck(loginResponse.sessionId)
                    if (!watchResponse.isError) {
                        viewModel.watchCount = watchResponse.resultsCount
                        checkError("Watches fetched: ${watchResponse.resultsCount}")
                    }
                }
            }
        } else {
            checkError("Username is empty", username.isEmpty()) ||
                    checkError("Password is empty", password.isEmpty())
        }
    }

    fun guestLoginClicked() {
        launch(UI) {
            doLogin(Inkbunny.loginApi.loginAsGuest(), "guest")
        }
    }

    fun logoutClicked() {
        val sessionId = viewModel.sessionId
        if (viewModel.sessionId != null) {
            launch(UI) {
                val response = Inkbunny.loginApi.logout(viewModel.sessionId!!).await()
                if (!checkError(response.errorMessage, response.isError)) {
                    viewModel.sessionId = null
                    viewModel.username = null
                }
            }
        } else {
            checkError(text = "Not logged in")
        }
    }

    private suspend fun doLogin(call: Deferred<LoginResponse>,
                                username: String,
                                onSuccess: ((LoginResponse) -> Unit)? = null): LoginResponse {
        val response = call.await()
        if (!checkError(response.errorMessage, response.isError)) {
            viewModel.sessionId = response.sessionId
            viewModel.username = username
            onSuccess?.invoke(response)
        }
        return response
    }

    private suspend fun doWatchlistCheck(sessionId: String,
                                         onSuccess: ((WatchlistResponse) -> Unit)? = null): WatchlistResponse {
        val response = Inkbunny.userApi.getUserWatchlist(sessionId).await()
        if (!checkError(response.errorMessage, response.isError)) {
            onSuccess?.invoke(response)
        }
        return response
    }

    private fun checkError(text: String?, check: Boolean = true, onError: (() -> Unit)? = null): Boolean {
        if (check) {
            Toast.makeText(this, text ?: "Unspecified error", Toast.LENGTH_SHORT).show()
            onError?.invoke()
        }
        return check
    }
}
