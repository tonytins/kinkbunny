package com.perrigogames.kinkbunny.android

import android.databinding.ObservableField

class LoginViewModel {

    val statusText = ObservableField("")

    var username: String? = null
        set(value) {
            field = value
            updateStatusLabel()
        }
    var sessionId: String? = null
        set(value) {
            field = value
            updateStatusLabel()
        }
    var watchCount: Int = 0
        set(value) {
            field = value
            updateStatusLabel()
        }

    init {
        updateStatusLabel()
    }

    private fun updateStatusLabel() {
        statusText.set("$USERNAME = $username\n$SID = $sessionId\n$WATCHES = $watchCount")
    }

    companion object {
        const val USERNAME = "USER"
        const val SID = "SID"
        const val WATCHES = "WATCHES"
    }
}